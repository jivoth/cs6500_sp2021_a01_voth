from matplotlib import pyplot as plt

with open('/Users/JonathanVoth/Desktop/CS_6500/cs6500_sp2021_a01_voth/Output02') as f:
    lines = f.readlines()
    year = [line.split()[0] for line in lines]
    y = [float(line.split()[2]) for line in lines]

x = [int(year[i].strip('[,')) for i in range(len(year))]

# Plotting the data
plt.scatter(x, y)
plt.title('Temperature Data (1901-1940)')
plt.xlabel('Year')
plt.ylabel('Temperature')

plt.show()