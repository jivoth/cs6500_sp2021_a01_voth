from mrjob.job import MRJob
from mrjob.step import MRStep
from textwrap import wrap


class Analysis(MRJob):

    def steps(self):
        return [
            MRStep(mapper=self.mapper, reducer=self.reducer1)
        ]

    def mapper(self, _, line):
        (USAF, WBAN, YRMODAHRMN, DIR, SPD, GUS, CLG, SKC, L, M, H, VSB, MW, MW, MW, MW, AW, AW, 
            AW, AW, W, TEMP, DEWP, SLP, ALT, STP, MAX, MIN, PCP01, PCP06, PCP24, PCPXX, SD) = line.split()

        try:
            num = wrap(YRMODAHRMN, 4)
            year = num[0]
            # count the number of times each SKC is reported
            yield ((int(year), SKC), 1)
        except ValueError:
            return
    
    def reducer1(self, key, counts):
        # get clear and overcast counts
        if key[1] == 'OVC' or key[1] == 'CLR':
            yield (key, sum(counts))

if __name__ == '__main__':
    Analysis.run()