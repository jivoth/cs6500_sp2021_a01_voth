from mrjob.job import MRJob
from mrjob.step import MRStep
import statistics
from textwrap import wrap


class Analysis(MRJob):

    def steps(self):
        return [
            MRStep(mapper=self.mapper, reducer=self.reducer)
        ]

    def mapper(self, _, line):
        data = line.split()
        x = wrap(data[1], 4)
        year = int(x[0])
        y = data[2:]
        
        try:
            for i in range(len(y)):
                yield (int(year), int(y[i].strip('[],')))
        except ValueError:
            pass

    def reducer(self, key, values):
        temps = list(values)
        n = len(temps)
        sorted_temps = sorted(temps)
        yield ((key, "min"), min(temps))
        yield ((key, "mean"), round(sum(temps)/n, 2))
        yield ((key, "med"), statistics.median(sorted_temps))
        yield ((key, "max"), max(temps))

if __name__ == '__main__':
    Analysis.run()