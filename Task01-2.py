from mrjob.job import MRJob
from mrjob.step import MRStep
from textwrap import wrap

# this function yields the station ID and years it was active
class MRMostUsedWord(MRJob):

    def mapper(self, _, line):
        data = line.split()
        x = wrap(data[1], 4)
        year = int(x[0])
        y = data[0]
        
        try:
            if year >= 1920 and year <= 1940:
                yield (y.strip('"[],'), year)
        except ValueError:
            pass

    def reducer(self, key, values):
        n = list(values)
        uniq = set(n)
        years = list(uniq)
        x = len(years)
        if x > 10:
            yield (key, years)


if __name__ == '__main__':
    MRMostUsedWord.run()