from matplotlib import pyplot as plt

with open('/Users/JonathanVoth/Desktop/CS_6500/cs6500_sp2021_a01_voth/Output04') as f:
    lines = f.readlines()
    y = [float(line.split()[1]) for line in lines]

x = [*range(1, 481, 1)]

plt.plot(x, y)
plt.title('Change in SLP (1901-1940)')
plt.xlabel('Time')
plt.ylabel('Sea Level Pressure')

plt.show()