from mrjob.job import MRJob
from mrjob.step import MRStep
from textwrap import wrap
import statistics


class Analysis(MRJob):

    def steps(self):
        return [
            MRStep(mapper=self.mapper, reducer=self.reducer)
        ]

    def mapper(self, _, line):
        (USAF, WBAN, YRMODAHRMN, DIR, SPD, GUS, CLG, SKC, L, M, H, VSB, MW, MW, MW, MW, AW, AW, 
            AW, AW, W, TEMP, DEWP, SLP, ALT, STP, MAX, MIN, PCP01, PCP06, PCP24, PCPXX, SD) = line.split()
            
        try:
            num = wrap(YRMODAHRMN, 6)
            yrmonth = num[0]
            yield (int(yrmonth), float(SLP))
        except ValueError:
            pass

    def reducer(self, key, values):
        slp = list(values)
        n = len(slp)
        yield (key, round(sum(slp)/n, 2))

if __name__ == '__main__':
    Analysis.run()