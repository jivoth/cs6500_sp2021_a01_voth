from mrjob.job import MRJob
from mrjob.step import MRStep
from textwrap import wrap

class FilterJob(MRJob):

    def steps(self):
        return [
            MRStep(mapper=self.mapper, reducer=self.reducer)
        ]

    def mapper(self, _, line):
        (USAF, WBAN, YRMODAHRMN, DIR, SPD, GUS, CLG, SKC, L, M, H, VSB, MW, MW, MW, MW, AW, AW, 
            AW, AW, W, TEMP, DEWP, SLP, ALT, STP, MAX, MIN, PCP01, PCP06, PCP24, PCPXX, SD) = line.split()

        try:
            num = wrap(YRMODAHRMN, 6)
            yrmonth = num[0]
            yield ((USAF, int(yrmonth)), (int(TEMP)))
        except ValueError:
            pass

    def reducer(self, key, values):
        f = list(key)
        data = list(values)
        if len(data) >= 2:
            yield (f, data)

if __name__ == '__main__':
    FilterJob.run()