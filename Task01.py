from mrjob.job import MRJob
from mrjob.step import MRStep
from textwrap import wrap


class MRMostUsedWord(MRJob):

    def steps(self):
        return [
            MRStep(mapper=self.mapper, reducer=self.reducer)
        ]

    def mapper(self, _, line):
        data = line.split()
        x = wrap(data[1], 4)
        year = int(x[0])
        y = data[0]
        
        try:
            if year >= 1920 and year <= 1940:
                yield (y.strip('"[],'), year)
        except ValueError:
            pass

    def reducer(self, key, values):
        n = list(values)
        uniq = set(n)
        x = len(uniq)
        yield (key, x)


if __name__ == '__main__':
    MRMostUsedWord.run()