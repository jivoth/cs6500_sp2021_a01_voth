from mrjob.job import MRJob
from mrjob.step import MRStep

# this function shows the stations that were active for more than 10 years
class MRMostUsedWord(MRJob):

    def steps(self):
        return [
            MRStep(mapper=self.mapper, reducer=self.reducer)
        ]

    def mapper(self, _, line):
        data = line.split()
        station = data[0].strip('"')
        
        yield (int(data[1]), station)

    def reducer(self, key, values):
        station = list(values)
        if key > 10:
            yield (key, station)


if __name__ == '__main__':
    MRMostUsedWord.run()