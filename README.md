# Jonathan Voth
## Master of Science in Analytics

The data used in this analysis is from the National Climate Data Center (NCDC) from the years 1901-1940. For the purposes of this analysis, a weather station was considered active/operable for a given year if they reported at least two observations per month for that year. If a station failed to meet this standard, then it was considered inactive and not chosen for the analysis for that year. A MapReduce funcion was written to only select those stations that meet the criteria. 

From 1920-1940, there were no stations that were active/operable the entire time. There were two stations, #029110 and #029750, that were active for 16 years, which was the most years a station was active during this time. These stations were both active from 1920-1926 and 1931-1939. The Stations_Year file shows a list of all the stations that were active/operable for more than 10 years and a list of all the operable years for each station.

From each operable station from 1901-1940, descriptive statistics were taken on the temperature data. The graph below shows the minimum, mean, median, and maximum temperature for each year.

![](Temp_plot.png)

From the graph, we can see that the maximum temperature steadily increases over time, and the minimum temperature steadily decreases as time progresses. The mean and median temperatures remains about the same across the years with a slight increase around 1930. This is interesting because the highs are increasing and the lows are decreasing.

I was also interested in learning about the trend of the sea level pressure (SLP) from 1901-1940. SLP changes with the seasons and can also be an indicator of a shift in weather. The graph below is a time-series plot showing the average reported SLP for each month in the time frame. The x-axis shows time in months.

![](SLP_plot.png)

It looks as though the SLP fluctuates between 990-1020 millibars from 1901-1925. Then, there is a drastic increase in the average SLP just before 1930. From then on, the SLP never dips below 1000 millibars, and the variation seems to decrease as time approaches 1940. This could be an indication of a significant change in weather around that time.

The final task looked at the distribution of the reported sky cover (SKC) for each year from 1901-1940. In particular, 'overcast' (OVC) was compared to 'clear' (CLR). A MapReduce function was implemented to count the number of times a specific SKC was reported. The output can be found in the Output03 file. In every year, OVC was reported more times than CLR. Sometimes, it was even reported up to 4x more than CLR was reported. Additionally, OVC was by far the most commonly reported SKC by all stations during the time frame. Even though this is a qualitative and subjective measure of the weather, it still provides good feedback on any changes in the weather over this time.